#!/bin/bash

backup_dir="/home/backup"

echo "Доступные бэкапы:"
ls $backup_dir/*.gz

echo "Введите имя файла бэкапа для восстановления (например, database_05-07-2024_18-56.sql.gz, где 05(месяц) 07(день) 2024(год) 18(часы) 56(минуты)):"
read backup_file

full_backup_path="$backup_dir/$backup_file"

if [ ! -f "$full_backup_path" ]; then
    echo "Файл бэкапа не найден: $full_backup_path"
    exit 1
fi

echo "Восстанавливается база данных из $backup_file в базу данных wordpress_db..."
gunzip < "$full_backup_path" | mysql -u root wordpress_db


if [ $? -eq 0 ]; then
    echo "Восстановление успешно завершено."
else
    echo "Ошибка восстановления."
fi
