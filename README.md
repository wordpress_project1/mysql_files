# Использованные пакеты и зависимости

- **mysql-server** - серверная часть БД MySQL.

---

# Две версии backup БД

- `database_05-07-2024_19-53.sql.gz` - версия первоначальной настройки
  - Логин - `qwe`
  - Пароль - `qwe`
- `database_05-07-2024_19-56.sql.gz` - версия готового сайта

# Cron-job автоматического бэкапа (1 раз в день)

```d
0 0 * * * root mysqldump -u root wordpress_db | gzip > /home/backup/database_$(date '+%m-%d-%Y_%H-%M').sql.gz
``` 

# Скрипт-файл для восстановления существующих БД

```bash
#!/bin/bash

backup_dir="/home/backup"

echo "Доступные бэкапы:"
ls $backup_dir/*.gz

echo "Введите имя файла бэкапа для восстановления (например, database_05-07-2024_18-56.sql.gz,>
read backup_file

full_backup_path="$backup_dir/$backup_file"

if [ ! -f "$full_backup_path" ]; then
    echo "Файл бэкапа не найден: $full_backup_path"
    exit 1
fi

echo "Восстанавливается база данных из $backup_file в базу данных wordpress_db..."
gunzip < "$full_backup_path" | mysql -u root wordpress_db


if [ $? -eq 0 ]; then
    echo "Восстановление успешно завершено."
else
    echo "Ошибка восстановления."
fi

```
